//
//  RNCShowPickerViewController.m
//  RNCPickeView
//
//  Created by kata on 13-12-17.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import "RNCShowPickerViewController.h"

@interface RNCShowPickerViewController ()

{
    UIPickerView *_pickerView1;
    UIPickerView *_pickerView2;
}
@property(nonatomic,strong) NSArray *array1;
@property(nonatomic,strong) NSArray *array2;

@end

@implementation RNCShowPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    self.array1 = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7"];
    
    self.array2 = @[@"a",@"b",@"c",@"d",@"e",@"f",@"g"];
    
    
    _pickerView1 = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 320, 200)];
    
    //选择标识
    _pickerView1.showsSelectionIndicator  = YES;
    
    _pickerView1.backgroundColor = [UIColor clearColor];
    
    _pickerView1.delegate = self;
    
    _pickerView1.dataSource = self;
    
    [self.view addSubview:_pickerView1];
    
    
   _pickerView2  = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 300, 320, 200)];
    
    _pickerView2.delegate = self;
    
    _pickerView2.dataSource = self;
    
//    _pickerView2.showsSelectionIndicator  = YES;

    [self.view addSubview:_pickerView2];
    
    
 
}

#pragma mark - pickerViewDataSource
// 块
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _pickerView1) {
        
        return 1;
        
    }else{
        
        return 2;
        
    }
}
// 行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (pickerView == _pickerView1) {
        return [self.array1 count];

    }else{
        
        return [self.array2 count];

    }
}

#pragma mark - pickerViewDelegate
// 宽
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (pickerView == _pickerView1) {
        
        return 200;
        
    }else{
        if (component == 0) {
            
            return 120;
            
        }else {
            
            return 200;

        }
    }
    
    
}

// 行距
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if (pickerView == _pickerView1) {
        
        return 40;
        
    }else{
        
        
        return 80;
        
    }
}
//

//内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == _pickerView1) {
        
        NSString *str1 = [self.array1 objectAtIndex:row];
        
        return str1;
        
    }else{
        
        
        NSString *str2 = [self.array2 objectAtIndex:row];

        return str2;
        
    }
    
    
}



- (void)test
{
    
    
}


/**
 *
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow
{
    
    
    
}
*/




//自定义里面的View
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    
//    if (pickerView == _pickerView1) {
//        
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 10)];
//        
//        view.backgroundColor = [UIColor redColor];
//        
//        [pickerView addSubview:view];
//        
//        return view;
//
//    }else{
//        
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
//        
//        view.backgroundColor = [UIColor redColor];
//        
//        [pickerView addSubview:view];
//        
//        return view;
//
//        
//        
//    }
//}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSLog(@"component == %d,   didSelectRow == %d ",component,row);
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
