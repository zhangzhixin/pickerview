//
//  RNCShowPickerViewController.h
//  RNCPickeView
//
//  Created by kata on 13-12-17.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RNCShowPickerViewController : UIViewController <UIPickerViewDataSource,
                                                            UIPickerViewDelegate>

@end
