//
//  main.m
//  RNCPickeView
//
//  Created by kata on 13-12-17.
//  Copyright (c) 2013年 kata. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RNCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RNCAppDelegate class]));
    }
}
